package nl.utwente.di.bookQuote;

import java.awt.print.Book;
import java.util.HashMap;

public class Quoter {
    HashMap<String,Double> book = new HashMap<>();
    public Quoter(){
        book.put("1",10.0);
        book.put("2",45.0);
        book.put("3",20.0);
        book.put("4",35.0);
        book.put("5",50.0);

    }
    public double getBookPrice(String isbn) {
        Double price = book.get(isbn);
        return price;
    }
}
